package com.ola.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ola.project.entity.CarIssue;

@Repository
public interface ICarIssueDAO extends JpaRepository<CarIssue, Long> {

}
