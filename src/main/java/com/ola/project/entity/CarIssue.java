package com.ola.project.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="test")
@Getter
@Setter
public class CarIssue {
	
	@Id
	@Column(name="id", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="email", nullable=false)
	private String email;
	
	@Column(name="timestamp", nullable=false)
	private String timestamp;
	
	@Column(name="car_number", nullable=false)
	private String carNumber;
	
	@Column(name="issue_category", nullable=false)
	private String issueCategory;
	

}
