package com.ola.project.service;

import java.util.List;

import com.ola.project.entity.CarIssue;

public interface ICarIssueService {
	
	public List<CarIssue> getCarIssues();

}
